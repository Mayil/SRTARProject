﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.HelloAR;

public class ObjectPlacement : MonoBehaviour
{
	public GameObject[] objSelect;

	public HelloARController objRef;

	public void ObjectSelection (int ObjNo)
	{
		objRef.ProductPrefab = objSelect [ObjNo];
		objRef.m_Placed = false;
        // Empty
    }

	public void MenuSelection (GameObject target)
	{
        //Active
        target.SetActive (!target.activeSelf);
	}

    public void StateChange(int value)
    {
        switch(value)
        {
            case 1:
            {
                    objRef.m_State = SessionState.Empty;
                    break;
            }
            case 2:
            {
                    objRef.m_State = SessionState.Active;
                    break;
            }
        }
    }
}
