﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GoogleARCore.HelloAR;

public class SpawnObjects : MonoBehaviour {
	public HelloARController obj;
	Camera cam;
	public bool selectSphere = false;
	public bool selectCube = false;
	public Text DebugText;
	[SerializeField]
	public GameObject cubePrefab, spherePrefab;
	// Use this for initialization
	void Start () {
		cam = obj.FirstPersonCamera;
	}
	
	//
	// Update is called once per frame
	void Update () {
		if ((Input.GetTouch(0)).phase==TouchPhase.Began)
		{
			DebugText.text = "TouchTrue";
			Ray mray = cam.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(mray, out hit, 100f))
			{
				DebugText.text = "RaycastHit" ;
				
				if (selectCube)
				{
					GameObject go=Instantiate(cubePrefab, hit.point + new Vector3(0,0.05f,0), Quaternion.identity);
					go.transform.parent=hit.transform;
					DebugText.text = "Cube"+go.activeSelf;
					selectCube = false;
				}
				if(selectSphere)
				{
					GameObject go=Instantiate(spherePrefab, hit.point + new Vector3(0, 0.05f, 0), Quaternion.identity);
					go.transform.parent = hit.transform;
					DebugText.text = "Sphere" + go.activeSelf;
					selectSphere = false;
				}
			}
		}
		
	}


	public void Cube()
	{
		selectCube = true;
		TransformScript.instance.ObjSelected = false;
	}

	public void Sphere()
	{
		selectSphere = true;
		TransformScript.instance.ObjSelected = false;
	}
}
