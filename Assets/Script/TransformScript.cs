﻿using UnityEngine;
using GoogleARCore.HelloAR;
using UnityEngine.UI;
using GoogleARCore;

public class TransformScript : MonoBehaviour {

	public static TransformScript instance;
	public HelloARController obj;

	public bool ObjSelected=false;
	public LayerMask mask,UImask;
	
	public GameObject target;
	GameObject transformObject;
	Camera cam;
	int TransformAction = 0;
	Vector3 scaleConstraint;
	public Text DebugText;


	void Start () {
		instance = this;
		cam = obj.FirstPersonCamera;
		scaleConstraint = new Vector3(0.01f, 0.01f, 0.01f);
	}

	Vector3 startPos, movedPos, updatePos,movingPos;
	void Update () {
		//if((!ObjSelected))
		{
			if ((Input.GetTouch(0).phase == TouchPhase.Began))
			{
				Ray mray = cam.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(mray, out hit, 100f, mask))
				{
					DebugText.text = "HitHit";
					transformObject = hit.transform.gameObject;
					ObjSelected = true;
					movingPos = transformObject.transform.position;
					Debug.Log(hit.transform.gameObject.name);
					Debug.Log(transformObject.transform.position);
					
					target.transform.position = transformObject.transform.position+new Vector3(0,0,0.5f);
					//target.transform.LookAt(cam.transform);
					target.transform.parent = transformObject.transform;
					target.SetActive(true);
					DebugText.text = "Canvas is "+target.activeSelf;
				}
			}
		}
		if(transformObject)
		{
			
			switch(TransformAction)
			{
				case 1:
					DebugText.text = "Case1"+ obj.m_State;
					if (obj.m_State == SessionState.Empty)
					{
						DebugText.text = "Entered Translate";
						/*if((Input.GetTouch(0).phase == TouchPhase.Moved))
						{
							Ray mray = cam.ScreenPointToRay(Input.mousePosition);
							RaycastHit hitplane;
							if (Physics.Raycast(mray, out hitplane, 100f))
							{
								movingPos = hitplane.point+new Vector3(0,0.05f,0);

								Debug.Log(movingPos);
							}
						}*/
						Touch touch;
						touch = Input.GetTouch(0);
						//if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
						{
							//DebugText.text = "Returning";
							//return;
						}
						
						TrackableHit hit;
						TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;
						if (touch.phase == TouchPhase.Moved)
						{
							if ((Session.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit)))
							{
								movingPos = hit.Pose.position + new Vector3(0, 0.05f, 0);
								DebugText.text = "Raycast on " + movingPos;
							}
						}
						movingPos.y = transformObject.transform.position.y;
						transformObject.transform.position = Vector3.Lerp(transformObject.transform.position, movingPos, 5 * Time.deltaTime);
					}
					break;
				case 2:
					
					if ((Input.GetTouch(0).phase == TouchPhase.Began))
					{

						startPos = Input.mousePosition;

					}
					else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
					{

						updatePos = Input.mousePosition;

					}
					else if ((Input.GetTouch(0).phase == TouchPhase.Ended))
					{

						startPos = updatePos = new Vector3(0, 0, 0);

					}
					movedPos = updatePos - startPos;
					movedPos.y = -movedPos.y;
					movedPos=movedPos.normalized;
					transformObject.transform.RotateAround(Vector3.down, movedPos.x *0.1f* Mathf.Deg2Rad*10);
					//transformObject.transform.RotateAround(Vector3.right, -movedPos.y * Mathf.Deg2Rad*10);
					
					break;
				case 3:
				
					if ((Input.GetTouch(0).phase == TouchPhase.Began))
					{

						startPos = Input.mousePosition;

					}
					else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
					{

						updatePos = Input.mousePosition;

					}
					else if((Input.GetTouch(0).phase == TouchPhase.Ended))
					{
						startPos = updatePos = new Vector3(0, 0, 0);
					}
					movedPos = updatePos - startPos;
					movedPos=movedPos.normalized;
					if (transformObject.transform.localScale.x > scaleConstraint.x)
					{
						transformObject.transform.localScale += new Vector3(movedPos.x, movedPos.x, movedPos.x) * 0.001f;
						//transformObject.transform.position += new Vector3(0, movedPos.y * 0.0003f, 0);
					}
					break;
				case 4:
					if ((Input.GetTouch(0).phase == TouchPhase.Began))
					{

						startPos = Input.mousePosition;

					}
					else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
					{

						updatePos = Input.mousePosition;

					}
					else if ((Input.GetTouch(0).phase == TouchPhase.Ended))
					{
						startPos = updatePos = new Vector3(0, 0, 0);
					}
					movedPos = updatePos - startPos;
					movedPos = movedPos.normalized;
					transformObject.transform.position += new Vector3(0,movedPos.y*0.005f,0);
					DebugText.text = "Height is:" + transformObject.transform.position.y;
					break;

			}
		}
	}

	//dsgds
	public void Move()
	{
		TransformAction = 1;
		DebugText.text="MoveSelected";
	}

	public void Rotate()
	{
		TransformAction = 2;
		DebugText.text="RotateSelected";
	}

	public void Scale()
	{
		TransformAction = 3;
		DebugText.text="ScaleSelected";
	}

	public void Height()
	{
		TransformAction = 4;
		DebugText.text = "HeightSelected";
	}
}
