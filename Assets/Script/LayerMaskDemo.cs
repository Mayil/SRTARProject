﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerMaskDemo : MonoBehaviour {

	int s=8, c=9;
	public LayerMask maskS,maskC;
	// Use this for initialization
	void Start () {
		maskS = (1 << s);
		maskC = (1 << c);

	}
	
	// Update is called once per frame
	void Update () {
		Ray mray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(mray,out hit,100f,maskS))
		{
			hit.transform.gameObject.GetComponent<Renderer>().material.color = new Color(1,1,0,1);
		}
		else if(Physics.Raycast(mray, out hit, 100f, maskC))
		{
			hit.transform.gameObject.GetComponent<Renderer>().material.color = new Color(1, 0, 0, 1);
		}
	}

	
}
