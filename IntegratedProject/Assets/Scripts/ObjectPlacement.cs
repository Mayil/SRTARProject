﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleARCore.HelloAR;

public class ObjectPlacement : MonoBehaviour
{
	public GameObject[] objSelect;

	public HelloARController objRef;
    public ObjectPlacement objSettings;

	private GameObject tempTarget;

	public void ObjectSelection (int ObjNo)
	{
		objRef.ProductPrefab = objSelect [ObjNo];
		objRef.m_Placed = false;
		
		// Active State Change
		objRef.StateChange(1);
		tempTarget.SetActive(false);

	}

	public void MenuSelection (GameObject target)
	{
        //UI State Change
		if(objRef.m_State==SessionState.Empty)
			objRef.StateChange(3);
		else if(objRef.m_State == SessionState.UI)
			objRef.StateChange(2);
		target.SetActive (!target.activeSelf);
		tempTarget = target;
	}

}
