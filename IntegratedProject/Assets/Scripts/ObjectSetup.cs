﻿using GoogleARCore;
using GoogleARCore.HelloAR;
using UnityEngine;
using UnityEngine.UI;

public class ObjectSetup : MonoBehaviour
{  

	//Five states to denote the state in which the object is present
     [System.Serializable]
	public enum ObjectState
	{
		Idle , TranslatePlane, TranslateHeight, Rotate , Scale
	};

	public HelloARController obj;
	public Text Console;
    public Camera oARCam;
    public Canvas  oCanvas;
    public ObjectState oStatus;

	//transformation variables
	Vector3 startPos, movedPos, updatePos, movingPos;

	void Start()
    {
        oStatus = ObjectState.Idle;
		obj = GameObject.FindObjectOfType<HelloARController>();
		Console = GameObject.Find("Debug Console").GetComponent<Text>();
		Console.text = "Debug Console";
    }   

	//for switching on/off the Transform Menu Canvas
    public void CanvasActive()
    {
        oCanvas = GetComponentInChildren<Canvas>();
        oCanvas.enabled=!oCanvas.enabled;
        oARCam = GameObject.Find("ARCore Device").GetComponentInChildren<Camera>();
        oCanvas.worldCamera = oARCam;
    }

	//State change for Planar Translation
	public void MoveObjectPlanar()
	{
		oStatus = ObjectState.TranslatePlane;
		IdleState();
	}

	//State change for Height Translation
    public void MoveObjectHeight()
    {
		oStatus = ObjectState.TranslateHeight;
		IdleState();
	}

	//State change for Rotation
    public void RotateObject()
    {
		oStatus = ObjectState.Rotate;
		IdleState();
	}

	//State change for Scaling
    public void ScaleObject()
    {
		oStatus = ObjectState.Scale;
		IdleState();
	}


	//Used For Managing the Object State for several objects present in the scene
	void IdleState()
	{
		ObjectSetup[] objs = FindObjectsOfType<ObjectSetup>();
		for (int i = 0; i < objs.Length; i++)
		{
			if (objs[i] != this)
				objs[i].oStatus = ObjectSetup.ObjectState.Idle;
		}
	}

    void Update()
    {
		//oCanvas.gameObject.transform.LookAt(obj.FirstPersonCamera.transform);



		//Tranformation Block
		switch (oStatus)
		{

			//Translation
			case ObjectState.TranslatePlane:
				Console.text = "Case1" + obj.m_State;
				if (obj.m_State == SessionState.Active)
				{
					Console.text = "Entered Translate";

					Touch touch;
					touch = Input.GetTouch(0);

					TrackableHit hit;
					TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinBounds | TrackableHitFlags.PlaneWithinPolygon;
					if (touch.phase == TouchPhase.Moved)
					{
						if ((Session.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit)))
						{
							movingPos = hit.Pose.position + new Vector3(0, 0.05f, 0);
							Console.text = "Raycast on " + movingPos;
							movingPos.y = this.transform.position.y;

							this.transform.position = Vector3.Lerp(this.transform.position, movingPos, 4 * Time.deltaTime);
						}
					}
					
				}
				break;


			//Rotation
			case ObjectState.Rotate:
				if ((Input.GetTouch(0).phase == TouchPhase.Began))
				{

					startPos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
				{

					updatePos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Ended))
				{

					startPos = updatePos = new Vector3(0, 0, 0);

				}
		
				movedPos = updatePos - startPos;
				movedPos.y = -movedPos.y;
				movedPos = movedPos.normalized;
				this.transform.RotateAround(Vector3.down, movedPos.x * 0.1f * Mathf.Deg2Rad * 10);
				oCanvas.transform.RotateAround(Vector3.down, -movedPos.x * 0.1f * Mathf.Deg2Rad * 10);
				break;

			//Scaling
			case ObjectState.Scale:
				if ((Input.GetTouch(0).phase == TouchPhase.Began))
				{

					startPos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
				{

					updatePos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Ended))
				{
					startPos = updatePos = new Vector3(0, 0, 0);
				}
				movedPos = updatePos - startPos;
				movedPos = movedPos.normalized;
				if (this.transform.localScale.y >= 0.1f)
				{
					Console.text = "Scale is:" + this.transform.localScale.x;
					this.transform.localScale += new Vector3(movedPos.y, movedPos.y, movedPos.y) * 0.002f;
					oCanvas.transform.localScale+= new Vector3(-movedPos.y, -movedPos.y, -movedPos.y) * 0.004f;
				}
				if(this.transform.localScale.y < 0.1f)
				{
					Console.text = "Scale is less than limit";
					this.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f) ;
				}
				break;


			//HeightTranslation
			case ObjectState.TranslateHeight:
				if ((Input.GetTouch(0).phase == TouchPhase.Began))
				{

					startPos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Moved))
				{

					updatePos = Input.mousePosition;

				}
				else if ((Input.GetTouch(0).phase == TouchPhase.Ended))
				{
					startPos = updatePos = new Vector3(0, 0, 0);
				}
				movedPos = updatePos - startPos;
				movedPos = movedPos.normalized;
				this.transform.position += new Vector3(0, movedPos.y * 0.001f, 0);
				oCanvas.transform.position += new Vector3(0, -movedPos.y * 0.001f, 0);
				Console.text = "Height is:" + this.transform.position.y;
				break;
		}
    }
}