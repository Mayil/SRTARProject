﻿using UnityEngine;

/// <summary>
/// This Script is for button management.
/// </summary>
public class ButtonScript : MonoBehaviour {

	public GameObject[] Buttons;
	ButtonScript[] ButtonOff;
	

	public void ClickEvent()
	{
		ButtonOff = FindObjectsOfType<ButtonScript>();
		foreach(ButtonScript b in ButtonOff)
		{
			foreach (GameObject obj in b.Buttons)
			{
				if (b != this)
					obj.SetActive(false);
				else
					obj.SetActive(!obj.activeSelf);
			}
		}
		

	}
}
